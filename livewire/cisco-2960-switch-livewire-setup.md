
# AXIA AUDIO 2101 SUPERIOR AVENUE CLEVELAND OHIO USA +1 216.241.7225 AXIAAUDIO.COM
Configuring the Cisco 2960 Switch for Livewire™
19 July, 2012 Rev 2.02

- [AXIA AUDIO 2101 SUPERIOR AVENUE CLEVELAND OHIO USA +1 216.241.7225 AXIAAUDIO.COM](#axia-audio-2101-superior-avenue-cleveland-ohio-usa-1-2162417225-axiaaudiocom)
  - [Introduction](#introduction)
  - [Serial Settings](#serial-settings)
  - [Initial Setup](#initial-setup)
  - [Configuration](#configuration)
  - [VLAN 1 interface configuration](#vlan-1-interface-configuration)
  - [Global IGMP configuration](#global-igmp-configuration)
  - [Global QoS configuration](#global-qos-configuration)
  - [Configuring FastEthernet and GigabitEthernet ports connecting to Axia Devices](#configuring-fastethernet-and-gigabitethernet-ports-connecting-to-axia-devices)
  - [Configuring ports connecting to other CISCO Catalyst switches](#configuring-ports-connecting-to-other-cisco-catalyst-switches)
  - [Check your configuration](#check-your-configuration)
  - [Save your configuration](#save-your-configuration)
  - [For more assistance:](#for-more-assistance)
  - [Supported Devices](#supported-devices)
  - [Resources](#resources)


## Introduction

Ethernet switches intended for use in constructing Axia Livewire IP-Audio networks must be
properly pre-configured to provide required multicast filtering and quality of service (QoS)
features. This document explains the required steps to configure Cisco 2960 switch specifically.
These commands may be applicable to other members of the Catalyst product line that support
these same commands.

For initial configuration of the device, you can use PuTTy, which is a free program that allows
you to connect to the switch either via a Telnet session, or through a serial port and console
session. If this is the first time the switch has been configured, you will have to connect to the
switch via a console cable and serial connection to enter into the Express Setup. During the initial
setup, you will assign the switch an IP address, which will then allow you to connect to the switch
via a Telnet session. PuTTy can be used on both x86 and x64 bit systems, and can be downloaded
here... http://www.putty.org/

IGMP is a function that you will be setting up on this switch, and for the IGMP querier process to
work properly the switch should be assigned the lowest IP address in your Livewire subnet.


Please refer to your Cisco switch manual for additional information about Express Setup and the
command line interface.

## Serial Settings

* 9600 baud
* 8 data bits
* no parity
* 1 stop bit
* no flow control

## Initial Setup

1. Press enter and then enter yes to start the initial configuration

```
         --- System Configuration Dialog ---

Enable secret warning
----------------------------------
In order to access the device manager, an enable secret is required
If you enter the initial configuration dialog, you will be prompted for the enable secret
If you choose not to enter the intial configuration dialog, or if you exit setup without setting the enable secret,
please set an enable secret using the following CLI in configuration mode-
enable secret 0 <cleartext password>
----------------------------------

Would you like to enter the initial configuration dialog? [yes/no]: yes
```

2. Enter yes to start the basic management setup

```
Would you like to enter basic management setup? [yes/no]: yes
```

3. Enter the hostname of the switch company_abreviation-sw## (i.e. keqq-sw02)

```
Enter host name [Switch]: keqq-sw02
```

4. Set the enable secret

```
The enable secret is a password used to protect access to
  privileged EXEC and configuration modes. This password, after
  entered, becomes encrypted in the configuration.
  Enter enable secret:
```

5. Set the enable password

```
The enable password is used when you do not specify an
  enable secret password, with some older software versions, and
  some boot images.
  Enter enable password:
```

6. Set the virtual terminal password

```
The virtual terminal password is used to protect
  access to the router over a network interface.
  Enter virtual terminal password:
  Enter virtual terminal password: changem3
```

7. Choose whether to setup snmp or not

```
Configure SNMP Network Management? [no]: no
```

8. Select interface to use for the management interface (i.e. Vlan1)

```
Interface              IP-Address      OK? Method Status                Protocol
Vlan1                  unassigned      YES unset  up                    up      
GigabitEthernet0/1     unassigned      YES unset  down                  down    
GigabitEthernet0/2     unassigned      YES unset  down                  down    
GigabitEthernet0/3     unassigned      YES unset  down                  down    
GigabitEthernet0/4     unassigned      YES unset  down                  down    
GigabitEthernet0/5     unassigned      YES unset  down                  down    
GigabitEthernet0/6     unassigned      YES unset  down                  down    
GigabitEthernet0/7     unassigned      YES unset  down                  down    
GigabitEthernet0/8     unassigned      YES unset  up                    up 

Enter interface name used to connect to the
management network from the above interface summary: Vlan1
```

9. Configure interface Vlan1 with static ip

```
Configuring interface Vlan1:
  Configure IP on this interface? [no]: yes
    IP address for this interface: 192.168.2.3
    Subnet mask for this interface [255.255.255.0] : 
    Class C network is 192.168.2.0, 24 subnet bits; mask is /24
```

10. Choose whether to create a cluster command switch (i.e. no)

```
Would you like to enable as a cluster command switch? [yes/no]: no
```

11. It will then display what the switch config will look like and will look similar to this.

```
The following configuration command script was created:

hostname keqq-sw2
enable secret 5 $1$GDVm$WkQQtP2QSPf91gTgvgf8Y.
enable password changem3
line vty 0 15
password changem3
no snmp-server
!
no ip routing

!
interface Vlan1
no shutdown
ip address 192.168.2.2 255.255.255.0
!
interface GigabitEthernet0/1
!
interface GigabitEthernet0/2
!
interface GigabitEthernet0/3
!
interface GigabitEthernet0/4
!         
interface GigabitEthernet0/5
!         
interface GigabitEthernet0/6
!         
interface GigabitEthernet0/7
!         
interface GigabitEthernet0/8
!         
end
```

12. If everything looks in the switch configuration select 2 otherwise select 0 or 1

```
[0] Go to the IOS command prompt without saving this config.
[1] Return back to the setup without saving this config.
[2] Save this configuration to nvram and exit.

Enter your selection [2]:
```

## Configuration

To determine if your switch has Advanced IP Services or Basic IP Services (look for IPBASE
string) software, use the “show ver” command.

```
show ver
```

To be able to make any configuration changes on the switch, you must enter Privileged User level
by executing the “enable” command:

```
enable
```

In Cisco switches, ports are named according to their speed: “FastEthernet” or “GigabitEthernet”.
When configuring your switch, you must use “Fa” or “GigabitEthernet” accordingly (e.g.: “fa0/1”
or “gi0/1”).The last digit specifies actual port number on the switch.

AXIA AUDIO 2101 SUPERIOR AVENUE CLEVELAND OHIO USA +1 216.241.7225 AXIAAUDIO.COM


<div style="page-break-after: always;"></div>

## VLAN 1 interface configuration

Enter your IP address and network mask.

```
config t
interface Vlan1
ip address 192.168.XXX.YYY 255.255.255.0
no shutdown
end
```

## Global IGMP configuration

One device needs to be plugged in during the configuration of Global IGMP

Enter the following instructions:

```
config t
ip igmp snooping querier
ip igmp snooping querier max-response-time 25
ip igmp snooping vlan 1 immediate-leave
ip igmp snooping querier timer expiry 205
end
```

## Global QoS configuration

Enter the following instructions:

```
config t
mls qos srr-queue input buffers 50 50
mls qos srr-queue input cos-map queue 1 threshold 1 5
mls qos srr-queue input cos-map queue 2 threshold 1 6 7
mls qos srr-queue output cos-map queue 1 threshold 1 6 7
mls qos srr-queue output cos-map queue 3 threshold 1 5
mls qos
end
```

AXIA AUDIO 2101 SUPERIOR AVENUE CLEVELAND OHIO USA +1 216.241.7225 AXIAAUDIO.COM


<div style="page-break-after: always;"></div>

## Configuring FastEthernet and GigabitEthernet ports connecting to Axia Devices

Enter the following instructions:

```
config t
interface fa0/x
switchport mode access
switchport nonegotiate
switchport block multicast
switchport voice vlan dot1p
srr-queue bandwidth share 30 20 25 25
srr-queue bandwidth shape 0 0 0 0
priority-queue out
spanning-tree portfast
mls qos trust cos
no ip igmp snooping tcn flood
end
```

* Where “x” is the port # to configure
* Use the RANGE command for multiple port configurations.
  * Syntax: interface range fa0/x – yy
* GigabitEthernet port configuration use syntax: gi0/x

## Configuring ports connecting to other CISCO Catalyst switches

Enter the following instructions:

```
config t
interface GigabitEthernet 0/x
switchport mode trunk
switchport block multicast
srr-queue bandwidth share 30 20 25 25
srr-queue bandwidth shape 0 0 0 0
priority-queue out
mls qos trust cos
no ip igmp snooping tcn flood
end
```

* If you see the error 'Command rejected: An interface whose trunk encapsulation is "Auto" can not be configured to "trunk" mode.' then run the following before 'switchport mode trunk'

```
switchport trunk encapsulation dot1q
```

* Where “x” is the port # to configure
* Use the RANGE command for multiple port configurations.
  * Syntax: interface range fa0/x – yy
* On a stackable switch use this syntax:
  * fa1/0/x or gi1/0/x

AXIA AUDIO 2101 SUPERIOR AVENUE CLEVELAND OHIO USA +1 216.241.7225 AXIAAUDIO.COM


<div style="page-break-after: always;"></div>

## Check your configuration

To check your switch’s complete configuration, enter:

```
show running-config
```

## Save your configuration

Do not forget to save your new configuration. Enter the following instructions:

```
copy running-config startup-config
```

## For more assistance:

If you have more questions about switch setup, contact Axia Support at support@telosalliance, or
at 216-622-0247.

## Supported Devices
* Cisco Catlyst 2960G
* Cisco Catlyst 3560 POE-8

## Resources
* [https://www.telosalliance.com/uploads/Axia%20Products/Support%20Documents/Tech%20Tips/configuring-the-cisco-2960-switch-for-livewire.pdf](https://www.telosalliance.com/uploads/Axia%20Products/Support%20Documents/Tech%20Tips/configuring-the-cisco-2960-switch-for-livewire.pdf)
* [https://www.telosalliance.com/uploads/Axia%20Products/Support%20Documents/Tech%20Tips/cisco3560-3750-switchconfig-for-livewire-rev4-1.pdf](https://www.telosalliance.com/uploads/Axia%20Products/Support%20Documents/Tech%20Tips/cisco3560-3750-switchconfig-for-livewire-rev4-1.pdf)
* [https://learningnetwork.cisco.com/s/question/0D53i00000Kt674CAB/command-rejected-an-interface-whose-trunk-encapsulation-is-auto-can-not-be-configured-to-trunk-mode](https://learningnetwork.cisco.com/s/question/0D53i00000Kt674CAB/command-rejected-an-interface-whose-trunk-encapsulation-is-auto-can-not-be-configured-to-trunk-mode)
* [https://www.cisco.com/c/dam/en/us/td/docs/switches/lan/catalyst2960/hardware/quick/guide/9368.pdf](https://www.cisco.com/c/dam/en/us/td/docs/switches/lan/catalyst2960/hardware/quick/guide/9368.pdf)

[Back](./readme.md)